<?php
	include('navbar.html');
?>
<!DOCTYPE HTML>
<html lang = "en">
<head>
	<meta charset="utf-8">
  	<meta name = "viewport" content = "width=device-width, initial-scale = 1">
  	<link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#city').keyup(function(){
				var query = $(this).val();
					if(query != '') {
							$.ajax({
							url:"search.php",
							method:"POST",
							data:{query:query},
							success:function(data){
								$('#citylist').fadeIn();
								$('#citylist').html(data);
							}
						});
					}
			});
			$(document).on('click', 'li', function(){
				$('#city').val($(this).text());
				$('#citylist').fadeOut();
			});
		});  
 </script>  
<style>
	#orderaddress {
		position: relative;
		max-width: 500px;
		top: 150px;
		right: 300px;
		border: solid white 1px;
	}
	#orderdata {
		position: relative;
		max-width: 400px;
		top: -375px;
		left: 300px;
		border: solid white 1px;
	}
	#pickupservice {
		border: solid red 2px;
	}
	</style>
	
	<script>
		/*function pickupservice() {
			if(document.getElementById('pickupcheck').checked) {
				document.getElementById('pickupservice').style.visibility = 'visible';
			}
			else
				document.getElementById('pickupservice').style.visibility = 'hidden';
		}*/
				function pickupservice() {
			if(document.getElementById('pickupcheck').checked) {
				document.getElementById('pickupservice').style.visibility = 'visible';
			}
			else
				document.getElementById('pickupservice').style.visibility = 'hidden';
		}

	</script>
</head>
<body>
	<div id = "orderaddress" class = "container">
		<div class = "panel panel-default">
			<div class = "panel-heading"><b>Order Details</b></div>
			<div class = "panel-body">
				<form action = "placeorder.php" id = "form1" method = "post">
					<div class = "form-group">
						<label for = "yourname">Name:</label>
						<input type = "text" class = "form-control" name = "yourname" id = "yourname" >
						<label for = "goods">Type of Goods:</label>
							<select class = "form-control" id = "goods">
							<option>Construction material</option>
							<option>Grains</option>
							<option>Grocery</option>
						</select>
						<label for = "youremail">Email:</label>
						<input type = "email"  class = "form-control" name = "youremail" id = "youremail" >
						<label for = "mobilenumber">Mobile Number:</label>
						<input type = "number" class = "form-control" name = "mobilenumber" id = "mobilenumber" maxlength = "10" size = "5" >
						<label for = "deladdress">Delivery Address</label>
						<input type = "textarea" class = "form-control" name = "deladdress" id = "daddress" placeholder = "address"  >
						<label for = "city">City</label><!-- add ajax here-->
						<input type = "text" class = "form-control" name = "city" id = "city" >
						<div id = "citylist"></div>
						<label for = "pincode">Pincode</label>
						<input type = "number" class = "form-control" name = "pincode" id = "pincode" maxlength = "6" size = "6"><br>
						<div class = "checkbox">
							<label><input type = "checkbox" name = "pickupcheck" id = "pickupcheck" onClick = "pickupservice()">Want pick up service</label>
						</div>
						
						<p id = "pickupservice" class = "form-control" style = "visibility : hidden">hello</p><br>
						<input type = "submit" name = "confirm" value = "confirm" data-toggle="modal" data-target="#myModal">
						<button onclick = "dis()">Try it</button>
					</div>

				</form>
     			</div>
		</div>
	</div>
<button type="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" onClick = "dis()">Open Modal</button>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>

        <div class="modal-body">
	<?php
	$hostname = "localhost";
	$username = "root";
	$password = "ak47gr";
	$dbname = "project";
	$connect = mysqli_connect($hostname, $username, $password, $dbname);
	$yourname = $_POST['yourname'];
	$yourmail = $_POST['yourmail'];
	$mobilenumber = $_POST['mobilenumber'];
	$deladdress = $_POST['deladdress'];
	$city = $_POST['city'];
	$pincode = $_POST['pincode'];
			if (isset($_POST['confirm'])) {
			
			echo "OrderID <br>" ;echo $i; 
			echo "Name <br>"; echo $pincode;
			echo "Email <br>";
			echo "MobieNumber <br>";
			echo "Delivery Address <br>";
			echo "City <br>";
			echo "Pincode <br>";
		

	}
	?>


        </div>
        <div class = "modal-footer">
          <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
	<div id = "orderdata" class = "container">
		<div class = "panel panel-default">
			<div class = "panel-heading"><b>Order Details</b></div>
			<div class = "panel-body">
				<form action = "myorder.php">
					<div class = "form-group">
						<label for = "goods">Quantity</label>
						<select class = "form-control" id = "goods">
							<option>Construction material</option>
							<option>Grains</option>
							<option>Grocery</option>
						</select>
						<label for = "goods">Quantity</label>
						<input type = "number" class = "form-control" name = "quantity" id = "quantity"><br>
						<!-- Suggestion of vehicle to be added according to need-->
						<div class = "checkbox">
							<label><input type = "checkbox" name = "urgentcheckbox" id = "urgentcheckbox">Urgent delivery needed</label>
						</div>
						<button type = "button" form = "orderdata" value = "submit">Calculate Rate</button>
					</div>
				</form>
			</div>
			
		</div>
	</div>
	
	
	
</body>
</html>
<?php
	$hostname = "localhost";
	$username = "root";
	$password = "ak47gr";
	$dbname = "project";
	$connect = mysqli_connect($hostname, $username, $password, $dbname);
	$yourname = $_POST['yourname'];
	$yourmail = $_POST['yourmail'];
	$mobilenumber = $_POST['mobilenumber'];
	$deladdress = $_POST['deladdress'];
	$city = $_POST['city'];
	$pincode = $_POST['pincode'];

	$query = "select *from ordertable";
		
	$result1 = mysqli_query($connect,$query);
	if(mysqli_num_rows($result1) >= 1) {
			echo "<table border = 2>";
			echo "<tr>";
			echo "<th>"; echo "OrderID"; echo "</th>";
			echo "<th>"; echo "Name";echo "</th>";
			echo "<th>"; echo "Email"; echo "</th>";
			echo "<th>"; echo "MobieNumber"; echo "</th>";
			echo "<th>"; echo "Delivery Address"; echo "</th>";
			echo "<th>"; echo "City"; echo "</th>";
			echo "<th>"; echo "Pincode"; echo "</th>";
			echo "</tr>";
			while($row = mysqli_fetch_array($result1)) {
				echo "<tr>";
				echo "<td>"; echo $i++; echo "</td>";

				echo "<td>"; echo $row["vehicletype"]; echo "</td>";
				echo "<td>"; echo $row["vehiclenumber"]; echo "</td>";
				echo "<td>"; echo $row["regnumber"]; echo "</td>";
				echo "<td>"; echo $row["loadcapacity"]; echo "</td>";
				echo "<td>"; echo $row["vehicleservice"]; echo "</td>";
				echo "</tr>";
			}
			
		}		
	
	
	
	?>
